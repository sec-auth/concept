
# Table of Contents

- [Authentication](AUTHENTICATION.md)
- Interfaces
  - [Authentication Challenge](interfaces/AUTHENTICATION_CHALLENGE.md)
  - [Authentication Token](interfaces/AUTHENTICATION_TOKEN.md)
  - [Common Claims](interfaces/COMMON_CLAIMS.md)

  