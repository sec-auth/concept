
# Common Claims

This is the initial list of common claims. Other common claims MAY be added later.

## Email

**Claim Name:** `email`

```typescript
interface EmailClaim {
    email: string;
}
```

**Trust Levels:**

| Trust Level | Approval |
| ----------- | -------- |
| Basic | Confirmed by an email to the given address |

## Address

**Claim Name:** `address`

**Schema:**

```typescript
interface AddressClaim {
    street: string;
    house: string;

    addition: string;

    zip: string;

    // ISO 3166-1 Alpha-2 country codes
    country: "AF" | "AX" | "GB" | .....
}
```

**Trust Levels:**

| Trust Level | Approval |
| ----------- | -------- |
| Basic | Confirmed by a Letter to the given address |
| High  | Confirmation through an governmental electronic id system |

## Natural Person Name

**Claim Name:** `personName`

```typescript
interface PersonNameClaim {
    firstName: string;
    middleName?: string[] | string;
    lastName: string;

    bornAs?: string;
}
```

