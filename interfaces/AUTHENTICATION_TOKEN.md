# Authentication Token

The AuthenticationToken is sent to the Authentication Endpoint of the Application by the user's Authentication App.

## Format

The Authentication is a JSON object with the following format:

```typescript
interface AuthenticationToken {
    certificate: JsonWebCertificate;
    token: JsonWebToken;
    claims?: JsonWebToken[]
}
```

The `certificate` is a JSON Web Certificate issued to the user.

The `token` is a standard JSON Web Token signed by the certificate. The token MUST include the following claims:

| Claim   | Description                                      |
| ------- | ------------------------------------------------ |
| `aud`   | The serviceUrl from the Authentication Challenge |
| `exp`   | JWT expiration time, see [RFC7519#section-4.1.4](https://www.rfc-editor.org/rfc/rfc7519#section-4.1.4) |
| `iat`   | JWT Issued At, see [RFC7519#section-4.1.6](https://www.rfc-editor.org/rfc/rfc7519#section-4.1.6) |
| `iss`   | The User ID, must match the common name from the JWC |
| `jti`   | JWT ID, see [RFC7519#section-4.1.7](https://www.rfc-editor.org/rfc/rfc7519#section-4.1.7) |
| `nbf`   | JWT Not Before, see [RFC7519#section-4.1.5](https://www.rfc-editor.org/rfc/rfc7519#section-4.1.5) |
| `sub`   | The User ID, must match the common name from the JWC |
| `nonce` | The nonce from the Authentication Challenge |

Other Claims MAY beused.

The `claims` MAY be included to transfer additional information to the Application. The claims parameter MUST NOT be an empty array.

## Claims Format

The `claims` array contains additional attributed describing the user. These attributes can be approved by third party issuers. 

**Example:**

> Given the user has an email adress. A 3rd party issuer can prove that the user owns this email adresss and create a claim that the user owns this email.
> The user can now give that claim to applications to prove he owns the email adress. 
> The application must trust the 3rd party issuer.

The Claims are JSON Web Tokens.

### Headers

The Claims have the following additional protected headers:

| Header | Protected? | Description |
| --- | --- | --- |
| jwcUrl | yes | A URL pointing to the used Json Web Certificate |

### Payload

The Claims have the following Payload:

| Claim | Description                                      |
| ----- | ------------------------------------------------ |
| `exp` | JWT expiration time, see [RFC7519#section-4.1.4](https://www.rfc-editor.org/rfc/rfc7519#section-4.1.4) |
| `iat` | JWT Issued At, see [RFC7519#section-4.1.6](https://www.rfc-editor.org/rfc/rfc7519#section-4.1.6) |
| `iss` | The Issuer, MUST match the common name from the issuer JWC |
| `jti` | JWT ID, see [RFC7519#section-4.1.7](https://www.rfc-editor.org/rfc/rfc7519#section-4.1.7) |
| `nbf` | JWT Not Before, see [RFC7519#section-4.1.5](https://www.rfc-editor.org/rfc/rfc7519#section-4.1.5) |
| `sub` | The User ID, must match the common name in the user JWC |
| `tlv` | The trust level, see below |
| `cid` | The claim ID, see below |
| `ct`  | The content of the claim, can be any json value |

### Claim Types

There are two kinds of claims: *Common Claims* and *Custom Claims*.

The *Custom Claims* are specific to an issuer. The `cid` and the format of the `ct` is custom. In this case the `cid` MUST have the following format: 

`${issuer_id}/${claim_id}`

The `issuer_id` and the `claim_id` MUEST be url encoded.

**Example:** `issuer.somedomain.com/something_specific`

There is a set of defined `cid`s with defined format for the `ct`. This are *Common Claims*. The `cid` of Common Claims is not prefixed with the user id.

The initial contents of the Common Claims registry can be found [here](COMMON_CLAIMS.md).

### Example

```json
{
    "exp": "1518239022",
    "iat": "1516239022",
    "nbf": "1516239022",

    "iss": "issuer.somedomain.com",
    "sub": "33a335a0-8eae-4c3d-8a35-18c4d43a48bb",

    "jti": "f0b1929a-eccd-4e3b-8bd4-40accbe46b92",

    "tlv": "Basic",
    "cid": "email",

    "ct": {
        "email": "someuser@somedomain.com"
    }
}
```

## Claims Trust Level

An issuer MUST prove the claims before the claim is signed. Different methods of approval can have different levels of security. In each claim the trust level of this approval must be given.

There are the following trust levels:

- None
- Basic
- High

Other trust levels MAY be added. If the application does not know the trust level, *None* MUST be assumed.

### Trust Level *None*

Claims with the trust level None are not approved at all. Claims with this trust level MUST NOT be used in any security sensitive way.

**Example:**

> If the user enters his adress into a formular on a website, the user can give any adress he likes.
> The adress is not proved to be the users real adress so the trust level is *None*.

### Trust Level *Basic*

Claims with the trust level *Basic* are proved by some basic mechanism. The mechanisms used should give a low level of security.

**Example:**

> The user enters his adress in a web formular.
> Before the claim is created, a letter with a verification code is sent to the user.
> The claim is created when the user enters the verification code into the application.

### Trust Level *High*

Claims with the trsut level *High* are secured by some mechanism that is robust to fraud. 

**Example:**

> The user does not enter his adress in some kind of formular.
> Instead the adress is taken from the user's electronis ID card.



