

# Authentication

If A User wants to authenticate with a web service, the web service must create an Authentication Challenge and submit it to the user's Authentication App.  

```mermaid

sequenceDiagram
    autonumber

    actor U as User
    participant App as Authentication App
    participant FE as Application Frontend
    participant BE as Application Backend

    U ->>+ FE: User visits Application
    FE ->>+ BE: Request Authentication Chanllenge
    BE -->> FE: Authentication Challenge

    FE -->>+ App: Transfer Authentication Challenge via some Front Channel
    deactivate FE
    Note over App,FE: Simplified: different ways

    App ->> App: Process Authentication Challenge
    App -->> U: Ask User for Consent to Claims
    U ->> App: Consent to some or all claims
    App ->> App: Create Authentication Token
    App ->> BE: Authentication Token
    BE ->> BE: Validate Authentication Token
    BE ->> BE: Update Session State
    BE --) FE: Notify Frontend (Optional)
    BE -->>- App: success response
    App -->>- U: Display success response

```
There are two different Alternatives to transfer the Authentication Challenge to the Authentication App. Both ways work over the front channel. 

- Open the App directly on the user's device
- Show a QR code to be scannt by the Authentication App

## Open App directly

```mermaid

sequenceDiagram

    actor U as User
    participant App as Authentication App
    participant FE as Application Frontend

    Note over U,FE: The Authentication Challenge is at the Front End
    
    FE -->> U: Display QR-Code with Authentication Challenge and "Open App" Button
    U ->> FE: User presses "Open App"
    FE -->> App: Redirect to App with Authentication Challenge in URL

    Note over U,FE: Now the App has the authentication Challenge and proceeds as above

```

## Trasfer via QR Code


```mermaid

sequenceDiagram

    actor U as User
    participant App as Authentication App
    participant FE as Application Frontend

    Note over U,FE: The Authentication Challenge is at the Front End
    
    FE -->> U: Display QR-Code with Authentication Challenge and "Open App" Button
    U ->> App: User opens Authentication App
    App ->> FE: Scan QR Code with App
    FE -->> App: Authentication Challenge in QR code

    Note over U,FE: Now the App has the authentication Challenge and proceeds as above

```
## Next

So there are two public interfaces that must be specified:

- The Authentication Endpoint of the Application Backend and the Format of the Authentication Token ([see](interfaces/AUTHENTICATION_TOKEN.md))
- The Authentication challenge and the transmission to the Authentication App ([see](interfaces/AUTHENTICATION_CHALLENGE.md))


